@public @buseventtype @Name("Exploration")
expression selectedActionValue{
	droneLog =>  case drone.qtable.action
	  when "east" then drone.qtable.position.east
	  when "west" then drone.qtable.position.west
	  when "south" then drone.qtable.position.south
	  when "north" then drone.qtable.position.north 
	  when "stay" then drone.qtable.position.stay 
	end
}
expression maxValue{
	droneLog => max(drone.qtable.position.east,
			drone.qtable.position.west,
			drone.qtable.position.south,
			drone.qtable.position.north,
			drone.qtable.position.stay) 
}

insert into Exploration 
select drone as Log
from pattern [every drone = DronesLog] as droneLog 
where maxValue(droneLog) != selectedActionValue(droneLog) 
			and maxValue(droneLog) != 0

