package CEP.uk.ac.aston.cep.drones.json;
import com.espertech.esper.runtime.client.*;

import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.internal.event.json.core.JsonEventBean;

public class StopperListener implements UpdateListener  {
	private final static Logger logger = LoggerFactory.getLogger(StopperListener.class);
	public long startMillis1 = System.currentTimeMillis();
	static String pattern ="";
	static String detectedCE; 
	static MqttClient mqttClient;
	static MemoryPersistence persistence = new MemoryPersistence();
	public static String topic = "Q_table_collection_exploration.json";
    static int qos             = 2; 
    static String broker       = "tcp://localhost:1883";
    static String clientId     = "CEPoutput";
  
	public StopperListener(String name) throws MqttException {
		pattern = name;
		mqttClient = new MqttClient(broker, clientId, persistence);
	    MqttConnectOptions connOpts = new MqttConnectOptions();
	    connOpts.setCleanSession(false);
	    System.out.println("Connecting listener to broker: "+broker);
        mqttClient.connect(connOpts);
        System.out.println("Connected");
	}

	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPRuntime runtime) {
		if (newEvents != null) {
			for (EventBean event : newEvents) {
				//System.out.println("Episode: "+event.get("episode")) ;
				//System.out.println("iteration: "+event.get("iteration")) ;
				//System.out.println("Actual: "+event.get("Actual")) ;
				//System.out.println("Average: "+event.get("Average")) ;
				//System.out.println("Count: "+event.get("Count")) ;
			}          
		}	
	}
	public void mqttpublish(String topic,String content) {
		MqttMessage messagePub = new MqttMessage(content.getBytes());
		messagePub.setQos(qos);
		try {
			mqttClient.publish(topic, messagePub);
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

}
