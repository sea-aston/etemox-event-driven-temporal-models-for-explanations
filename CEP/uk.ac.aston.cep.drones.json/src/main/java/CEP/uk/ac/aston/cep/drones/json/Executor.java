package CEP.uk.ac.aston.cep.drones.json;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.fireandforget.EPFireAndForgetPreparedQuery;
import com.espertech.esper.common.client.fireandforget.EPFireAndForgetQueryResult;
import com.espertech.esper.common.client.json.minimaljson.Json;
import com.espertech.esper.common.client.json.minimaljson.JsonObject;
import com.espertech.esper.common.client.json.minimaljson.JsonParser;
import com.espertech.esper.common.client.json.minimaljson.JsonValue;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompiler;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.*;

import CEP.uk.ac.aston.cep.drones.json.GlobalListener;
import utils.EsperUtils;

public class Executor {
	public static EPRuntime epRuntime;
	public static String topicQtable = "Q_table_collection.json";
	static int qos = 0;
	static String broker = "tcp://localhost:1883";
	static String clientId = "CEP";
	static boolean flag = false;
	static MemoryPersistence persistence = new MemoryPersistence();
	static String message;
	static MqttClient mqttClient;
	public static EPDeployment samplerPattern;
	public static EPDeployment samplerPattern100;
	public static EPDeployment samplerPattern500;
	public static EPDeployment explorationPattern;
	public static EPDeployment explora2Pattern;
	public static EPDeployment exploitationPattern;
	public static EPDeployment exploita2Pattern;
	public static EPDeployment stopperPattern;
	public static EPDeployment patientPattern;
	public static EPDeployment freqPattern;
	public static EPDeployment patientFreqPattern;
	public static EPDeployment contextPattern;
	public static EPDeployment rewardPatternER;
	public static EPDeployment rewardPatternET;
	public static String auxState;
	public static String[] sXY = null;
	private static int episode, step, drone_number;
	public static double east, west, south, north, stay;

	// Declaring Esper schema Json drones
	static String schema = "@public @buseventtype @JsonSchema(className='DronesEntry') create json schema  DronesLog()";
	static String dronesWindow = "@public @buseventtype create window dronesWindow#length(2000) as DronesLog";
	static String mergeWindow = "@public @buseventtype on DronesLog merge dronesWindow insert select *";
	static String context = "@public @buseventtype create context mostFreqByDrone "
			+ "partition by drone.drone_number from mostFreq "
			+ "initiated by mostFreq as drone "
			+ "terminated by PatienceFreq";
			
	public static void main(String[] args) throws EPCompileException, EPDeployException, MqttException, IOException {
		// Create new Esper instance
		new EsperUtils();
		epRuntime = EsperUtils.getEpRuntime();
		CompilerArguments epCompilerArguments = EsperUtils.getCompilerArguments();
		epCompilerArguments.getPath().add(epRuntime.getRuntimePath());
		EsperUtils.addNewSchema(schema);
		EsperUtils.deployNewEventPattern(dronesWindow);
		EsperUtils.deployNewEventPattern(mergeWindow);
		

		// Declaring patterns
		
		freqPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/mostFreq.epl")).collect(Collectors.joining("\n")));
		EPStatement stopperStatement = epRuntime.getDeploymentService().getStatement(freqPattern.getDeploymentId(),"mostFreq");
		stopperStatement.addListener(new StopperListener("mostFreq pattern"));
		
		patientFreqPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/PatienceFreq.epl")).collect(Collectors.joining("\n")));
		EPStatement frequentStatement = epRuntime.getDeploymentService().getStatement(patientFreqPattern.getDeploymentId(),"PatienceFreq");
		frequentStatement.addListener(new GlobalListener("PatienceFreq pattern"));
		
		/*
		EsperUtils.deployNewEventPattern(context);
		contextPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/ContextPattern.epl")).collect(Collectors.joining("\n")));
		EPStatement contextStatement = epRuntime.getDeploymentService().getStatement(contextPattern.getDeploymentId(),"contextPattern");
		contextStatement.addListener(new GlobalListener("contextPattern"));
		
		stopperPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/StopperAvg.epl")).collect(Collectors.joining("\n")));
		EPStatement stopperAvgStatement = epRuntime.getDeploymentService().getStatement(stopperPattern.getDeploymentId(),"Stopper");
		stopperAvgStatement.addListener(new StopperListener("Stopper pattern"));
		
		patientPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Patience.epl")).collect(Collectors.joining("\n")));
		EPStatement frequentAvgStatement = epRuntime.getDeploymentService().getStatement(patientPattern.getDeploymentId(),"Patience");
		frequentAvgStatement.addListener(new PatienceListener("Patience pattern"));

		String frequentPattern1 = (Files.lines(Paths.get("resources/AvgFireForget.epl")).collect(Collectors.joining("\n"))).toString();
		EPCompiled compiled = EPCompilerProvider.getCompiler().compileQuery(frequentPattern1, epCompilerArguments); 
		EPFireAndForgetPreparedQuery result = epRuntime.getFireAndForgetService().prepareQuery(compiled);
		for (EventBean row : ((EPFireAndForgetQueryResult) result).getArray()) {
			  System.out.println("name=" + row.get("name"));
			}
		*/
		
		samplerPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Sampler.epl")).collect(Collectors.joining("\n")));
		EPStatement samplerStatement = epRuntime.getDeploymentService().getStatement(samplerPattern.getDeploymentId(),"Sampler");
		samplerStatement.addListener(new GlobalListener("Sampler pattern 10"));
		/*
		samplerPattern100 = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Sampler100.epl")).collect(Collectors.joining("\n")));
		EPStatement samplerStatement100 = epRuntime.getDeploymentService()
				.getStatement(samplerPattern100.getDeploymentId(), "Sampler100");
		samplerStatement100.addListener(new GlobalListener("Sampler pattern 100"));

		samplerPattern500 = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Sampler500.epl")).collect(Collectors.joining("\n")));
		EPStatement samplerStatement500 = epRuntime.getDeploymentService()
				.getStatement(samplerPattern500.getDeploymentId(), "Sampler500");
		samplerStatement500.addListener(new GlobalListener("Sampler pattern 500"));
	*/
		explorationPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Exploration.epl")).collect(Collectors.joining("\n")));
		EPStatement explorationStatement = epRuntime.getDeploymentService()
				.getStatement(explorationPattern.getDeploymentId(), "Exploration");
		explorationStatement.addListener(new GlobalListener("Exploration pattern"));
		
		rewardPatternER = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/RewardsExploration.epl")).collect(Collectors.joining("\n")));
		EPStatement rewardStatement = epRuntime.getDeploymentService()
				.getStatement(rewardPatternER.getDeploymentId(), "RewardsExploration");
		rewardStatement.addListener(new ExplorationListener("RewardsExploration pattern"));
		
		exploitationPattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Exploitation.epl")).collect(Collectors.joining("\n")));
		EPStatement exploitationStatement = epRuntime.getDeploymentService()
				.getStatement(exploitationPattern.getDeploymentId(), "Exploitation");
		exploitationStatement.addListener(new GlobalListener("Exploitation pattern"));
		
		rewardPatternET = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/RewardsExploitation.epl")).collect(Collectors.joining("\n")));
		EPStatement rewardStatementET = epRuntime.getDeploymentService()
				.getStatement(rewardPatternET.getDeploymentId(), "RewardsExploitation");
		rewardStatementET.addListener(new ExplorationListener("RewardsExploitation pattern"));
		
		
		/*
		explora2Pattern = EsperUtils.deployNewEventPattern(
				Files.lines(Paths.get("resources/Explora2.epl")).collect(Collectors.joining("\n")));
		EPStatement explora2Statement = epRuntime.getDeploymentService()
				.getStatement(explora2Pattern.getDeploymentId(), "Explora2");
		explora2Statement.addListener(new ExplorationListener("Exploration2 pattern"));
		*/
		//
		// Connect to the broker
		mqttClient = new MqttClient(broker, clientId, persistence);
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(false);
		// Set Callbacks
		mqttClient.setCallback(new MqttCallback() {
			public void connectionLost(Throwable cause) {
				// Called when the client lost the connection to the broker
				System.out.println("Connection to localhost broker lost! " + cause.getMessage());
			}

			public void messageArrived(String topic1, MqttMessage message1) throws Exception {
				if (topic1.equals(topicQtable)) {
					try {
						String json = parser(message1);
						epRuntime.getEventService().sendEventJson(json.toString(), "DronesLog");
					} catch (Exception e) {
						System.out.println(e);
					}
				}
			}

			public void deliveryComplete(IMqttDeliveryToken token) {
				// Called when a outgoing publish is complete
			}
		});
		// Conecting and subscribing to MQTT
		System.out.println("Connecting to broker: " + broker);
		mqttClient.connect(connOpts);
		System.out.println("Connected");
		System.out.println("Subscribing client to topic: " + topicQtable);
		mqttClient.subscribe(topicQtable, 0);
		System.out.println("Subscribed");

		// Sending Test Events to ESPER
		/*
		 * JsonObject jsonInput = new JsonObject(); FileReader reader; try { reader =
		 * new FileReader("resources/input.json");
		 * 
		 * @SuppressWarnings("static-access") JsonObject
		 * jsonMessage=jsonInput.readFrom(reader); //String
		 * json=parser(jsonMessage.asString());
		 * 
		 * String event1 = jsonMessage.toString();
		 * epRuntime.getEventService().sendEventJson(event1, "DronesLog"); } catch
		 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
	}

	public static String parser(MqttMessage mqttMessage) {
		String message = new String(mqttMessage.getPayload());
		message = ((String) message).replace("\'", "\"");
		JsonObject message1 = Json.parse(message).asObject();
		episode = message1.get("episode").asInt();
		step = message1.get("step").asInt();
		drone_number = message1.get("drone_number").asInt();
		JsonObject qTable = (JsonObject) message1.get("qtable");
		String action = (String) qTable.get("action").asString();
		JsonObject reward = (JsonObject) qTable.get("reward");
		Integer drone0Reward = reward.get("0").asInt();
		Integer drone1Reward = reward.get("1").asInt();
		Integer totalReward = reward.get("total").asInt();
		JsonObject state = (JsonObject) qTable.get("state");
		String drone0state = state.get("drone 0").asString();
		String drone1state = state.get("drone 1").asString();
		int droneX, droneY;
		if (drone_number == 0) {
			auxState = drone0state;
			sXY = auxState.substring(1, auxState.length() - 1).split(",");
			droneX = (int) Double.parseDouble(sXY[0].trim());
			droneY = (int) Double.parseDouble(sXY[1].trim());
			auxState = String.format("position: (%d, %d)", droneX, droneY);
		} else {
			auxState = drone1state;
			sXY = auxState.substring(1, auxState.length() - 1).split(",");
			droneX = (int) Double.parseDouble(sXY[0].trim());
			droneY = (int) Double.parseDouble(sXY[1].trim());
			auxState = String.format("position: (%d, %d)", droneX, droneY);
		}
		JsonObject position = (JsonObject) qTable.get(auxState);
		east = position.get("east").asDouble();
		west = position.get("west").asDouble();
		south = position.get("south").asDouble();
		north = position.get("north").asDouble();
		stay = position.get("stay").asDouble();

		JsonObject SINR = (JsonObject) qTable.get("SINR");
		return "{\"episode\":" + episode + ",\"step\":" + step + ", " + "\"drone_number\":" + drone_number
				+ ",\"droneX\":" + droneX + ",\"droneY\":" + droneY + ",\"qtable\":{" + "\"" + auxState + "\" : {"
				+ "\"east\":" + east + ", \"west\" :" + west + ", \"south\":" + south + ", \"north\":" + north
				+ ", \"stay\":" + stay + "}," + "\"position\":{" + "\"east\":" + east + ", \"west\" :" + west
				+ ", \"south\":" + south + ", \"north\":" + north + ", \"stay\":" + stay + "}," + "\"SINR\":" + SINR
				+ "," + "\"state\":{\"drone0\":\"" + drone0state + "\", \"drone1\":\"" + drone1state + "\"},"
				+ "\"action\":\"" + action + "\"," + " \"reward\":{\"drone0\": " + drone0Reward + ", \"drone1\":"
				+ drone1Reward + ", \"total\":" + totalReward + "}}}";
	}
}
