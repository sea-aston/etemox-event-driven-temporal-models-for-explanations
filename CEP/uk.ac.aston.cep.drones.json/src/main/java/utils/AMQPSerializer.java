package utils;

import com.espertech.esper.common.internal.util.SerializerUtil;
import com.espertech.esperio.amqp.AMQPToObjectCollector;
import com.espertech.esperio.amqp.AMQPToObjectCollectorContext;
public class AMQPSerializer implements AMQPToObjectCollector {

    public void collect(AMQPToObjectCollectorContext context) {
        System.out.println("PROCESSING MESSAGE: " + new String(context.getBytes()));
        context.getEmitter().submit(SerializerUtil.objectToByteArr(context.getBytes()));
    }
}