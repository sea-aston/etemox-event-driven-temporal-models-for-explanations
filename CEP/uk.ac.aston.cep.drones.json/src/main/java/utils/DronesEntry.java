package utils;

import java.util.Map;

public class DronesEntry {

	public DronesEntry() {
	}

	public int episode;
	public int step;
	public String drone_number;
	public String agent;
	public int droneX, droneY;
	public Qtable qtable;

	@Override
	public String toString() {
		String auxState = String.format("position: (%d, %d)", droneX, droneY);
		Map<String, Object> SINR = qtable.SINR;
		String auxiliar = null;
		for (String user : SINR.keySet()) {
			auxiliar += "\"" + user + "\" : \"" + (String) SINR.get(user) + "\",";
		}
		String auxSINR = auxiliar.toString();
		auxSINR = auxSINR.substring(auxSINR.indexOf("user") - 1, auxSINR.length() - 1);
		return "{\"episode\":" + episode + ",\"step\":" + step + ", " + "\"drone_number\":" + drone_number + ","+"\"agent\":" +agent+ ", "
				+ "\"qtable\":{" + "\"" + auxState + "\" : {" + "\"east\":" + qtable.position.east + ", \"west\" :"
				+ qtable.position.west + ", \"south\":" + qtable.position.south + ", \"north\":" + qtable.position.north
				+ ", \"stay\":" + qtable.position.stay + "}," + "\"SINR\":{" + auxSINR + "},"
				+ "\"state\":{\"drone 0\":\"" + qtable.state.drone0 + "\", \"drone 1\":\"" + qtable.state.drone1
				+ "\"}," + "\"action\":\"" + qtable.action + "\"," + " \"reward\":{\"0\": " + qtable.reward.drone0
				+ ", \"1\":" + qtable.reward.drone1 + ", \"total\":" + qtable.reward.total + "}}}";
	}
}