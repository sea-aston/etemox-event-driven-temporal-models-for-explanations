package uk.ac.aston.hawk.mongodb.drones.parser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.DoubleListMeasurement;
import uk.ac.aston.stormlog.DoubleMeasurement;
import uk.ac.aston.stormlog.IntegerMeasurement;
import uk.ac.aston.stormlog.Log;
import uk.ac.aston.stormlog.Measure;
import uk.ac.aston.stormlog.Observation;
import uk.ac.aston.stormlog.StormlogFactory;
import uk.ac.aston.stormlog.StringMeasurement;

/**
 * Converts the contents of an initial settings document to the STORM log metamodel. 
 */
public class InitialSettingsConverter {

	public static final String ACTION_STAY = "Stay";
	public static final String ACTION_DOWN = "Down";
	public static final String ACTION_UP = "Up";
	public static final String ACTION_LEFT = "Left";
	public static final String ACTION_RIGHT = "Right";
	public static final String MEASURE_DRONE = "Drone";
	public static final String MEASURE_EPISODE = "Episode";
	public static final String MEASURE_STEP = "Step";
	public static final String MEASURE_DRONEPOS = "Drones position state";
	public static final String MEASURE_SINR = "Global SINR";
	public static final String MEASURE_INITIAL_DRONEPOS = "Initial drone positions";
	public static final String MEASURE_REWARD_TOTAL = "Reward total";
	public static final String MEASURE_REWARD_DRONE = "Reward by drone";

	private static final StormlogFactory FACTORY = StormlogFactory.eINSTANCE;
	

	private final Document doc;
	private final Map<String, Measure> measures = new HashMap<>();
	private Log log;

	public InitialSettingsConverter(File f) throws IOException {
		this(new String(Files.readAllBytes(f.toPath())));
	}

	public InitialSettingsConverter(String json) {
		this.doc = Document.parse(json);
	}

	public synchronized Log getLog() {
		if (log == null) {
			createLog();
		}
		return log;
	}

	private void createLog() {
		this.log = FACTORY.createLog();
		addActions();
		addMeasures();
		addObservations();
	}

	private void addActions() {
		@SuppressWarnings("unchecked")
		List<List<Integer>> lActions = (List<List<Integer>>) doc.get("possible_actions");

		for (List<Integer> lAction : lActions) {
			final int dx = lAction.get(0);
			final int dy = lAction.get(1);

			Action action = FACTORY.createAction();
			if (dx > 0)      action.setName(ACTION_RIGHT);
			else if (dx < 0) action.setName(ACTION_LEFT);
			else if (dy > 0) action.setName(ACTION_UP);
			else if (dy < 0) action.setName(ACTION_DOWN);
			else action.setName(ACTION_STAY);

			log.getActions().add(action);
		}
	}

	protected void addMeasures() {
		String[][] measures = {
			{ "random_seed", "Random seed" },
			{ "transmit_power", "Transmit power" },
			{ "discount_factor", "Discount factor" },
			{ "sinr_threshold", "SINR threshold" },
			{ "x_min", "xMin" },
			{ "x_max", "xMax" },
			{ "y_min", "yMin" },
			{ "y_max", "yMax" },
			{ "total_episodes", "Episodes number" },
			{ "num_drones", "Drones number" },
			{ "num_users", "Users number" },
			{ "drone_user_capacity", "Number of users per drone" },
			{ "carrier_frequency", "Carrier frequency" },
			{ "learning_rate", "Learning rate" },
			{ "user_positions", "User positions" },
			{ "drone_positions", MEASURE_INITIAL_DRONEPOS },
			{ "qtable_SINR", MEASURE_SINR },
			{ "qtable_drone_positions", MEASURE_DRONEPOS },
			{ "qtable_step", MEASURE_STEP },
			{ "qtable_drone", MEASURE_DRONE },
			{ "qtable_episode", MEASURE_EPISODE },
			{ "reward_total", MEASURE_REWARD_TOTAL },
			{ "reward_by_drone", MEASURE_REWARD_DRONE }
		};

		for (String[] entry : measures) {
			Measure measure = FACTORY.createMeasure();
			measure.setName(entry[1]);
			this.measures.put(entry[0], measure);
			log.getMeasures().add(measure);
		}
	}

	private void addObservations() {
		final Observation obs = FACTORY.createObservation();
		obs.setDescription("Initial settings");
		log.getObservations().add(obs);

		for (String key : doc.keySet()) {
			final Measure measure = measures.get(key);
			if (measure == null) continue;

			Object value = doc.get(key);
			if (value instanceof Float || value instanceof Double) {
				final double dValue = ((Number)value).doubleValue();
				DoubleMeasurement m = FACTORY.createDoubleMeasurement();
				m.setMeasure(measure);
				m.setValue(dValue);
				obs.getMeasurements().add(m);
			} else if (value instanceof Number) {
				final int iValue = ((Number)value).intValue();
				IntegerMeasurement m = FACTORY.createIntegerMeasurement();
				m.setMeasure(measure);
				m.setValue(iValue);
				obs.getMeasurements().add(m);
			} else if (value instanceof String) {
				StringMeasurement m = FACTORY.createStringMeasurement();
				m.setMeasure(measure);
				m.setValue(value.toString());
				obs.getMeasurements().add(m);
			} else if (value instanceof Document) {
				DoubleListMeasurement m = FACTORY.createDoubleListMeasurement();
				m.setMeasure(measure);
				ParsingUtils.parseTuplesSubdocument((Document) value, m);
				obs.getMeasurements().add(m);
			} else {
				throw new IllegalArgumentException(String.format(
					"Unknown value type %s (key %s)",
					value.getClass(), key));
			}
		}
	}


	
}
