/*******************************************************************************
 * Copyright (c) 2019 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.hawk.mongodb;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.VcsChangeType;
import org.eclipse.hawk.core.VcsCommit;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.VcsRepositoryDelta;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

/**
 * Allows Hawk to access database records in a MongoDB collection. The documents
 * must maintain a timestamp of their last update, named
 * {@link #ATTR_LAST_UPDATED}. Document deletions must happen while Hawk is
 * connected to the MongoDB database.
 *
 * This is a generic solution developed before we had the specific format used
 * for the drone case study. The drone-specific Hawk connector is in the
 * <code>org.hawk.mongodb.drones</code> project.
 */
public class MongoCollection extends AbstractMongoConnector {

	public static final String ATTR_LAST_UPDATED = "lastUpdated";

	private String mongoCollectionName;
	private static final String FIRST_REV = "0";

	private com.mongodb.client.MongoCollection<Document> mongoCollection;

	private Set<String> previousObjectIDs;

	@Override
	public String getHumanReadableName() {
		return "MongoDB Collection";
	}

	@Override
	public String getCurrentRevision() throws Exception {
		final FindIterable<Document> cursor = mongoCollection
			.find(new Document())
			.projection(new Document(ATTR_LAST_UPDATED, 1))
			.sort(Sorts.descending(ATTR_LAST_UPDATED))
			.limit(1);

		for (Document d : cursor) {
			return d.getLong(ATTR_LAST_UPDATED) + "";
		}

		return FIRST_REV;
	}

	@Override
	public String getFirstRevision() throws Exception {
		return FIRST_REV;
	}

	@Override
	public Collection<VcsCommitItem> getDelta(String startRevision) throws Exception {
		return getDelta(startRevision, null).getCompactedCommitItems();
	}

	@Override
	public VcsRepositoryDelta getDelta(String startRevision, String endRevision) throws Exception {
		// TODO: use a real iterable instead of an ArrayList, to save memory
		final String currentRevision = getCurrentRevision();
		final List<VcsCommit> commits = new ArrayList<>();
		final VcsRepositoryDelta delta = new VcsRepositoryDelta(commits);
		delta.setManager(this);

		// Detect deleted documents since last check
		final Set<String> allObjectIDs = new HashSet<String>();
		for (Document d : mongoCollection.find().projection(new Document("_id", 1))) {
			allObjectIDs.add(d.getObjectId("_id").toString());
		}

		VcsCommit deleteCommit = null;
		for (String prevID : previousObjectIDs) {
			if (!allObjectIDs.contains(prevID)) {
				if (deleteCommit == null) {
					deleteCommit = new VcsCommit();
					deleteCommit.setAuthor("Mongo driver");
					deleteCommit.setJavaDate(new Date());
					deleteCommit.setMessage("Deleting documents");
					deleteCommit.setRevision(currentRevision);
					commits.add(deleteCommit);
				}

				VcsCommitItem c = new VcsCommitItem();
				c.setChangeType(VcsChangeType.DELETED);
				c.setCommit(deleteCommit);
				c.setPath("/" + prevID);
				deleteCommit.getItems().add(c);
			}
		}
		previousObjectIDs = allObjectIDs;

		// Use the update timestamps to find added/updated documents
		VcsCommit updateCommit = null;
		for (Document d : mongoCollection.find(Filters.gt(ATTR_LAST_UPDATED, Long.parseLong(startRevision)))) {
			if (updateCommit == null) {
				updateCommit = new VcsCommit();
				updateCommit.setAuthor("Mongo driver");
				updateCommit.setJavaDate(new Date());
				updateCommit.setMessage("Updating documents");
				updateCommit.setRevision(currentRevision);
				commits.add(updateCommit);
			}

			VcsCommitItem c = new VcsCommitItem();
			c.setChangeType(VcsChangeType.UPDATED);
			c.setCommit(updateCommit);
			c.setPath("/" + d.getObjectId("_id").toString());
			updateCommit.getItems().add(c);
		}

		return delta;
	}

	@Override
	public File importFile(String revision, String path, File optionalTemp) {
		final String oid = path.substring(1);

		try {
			for (Document d : mongoCollection.find(new Document("_id", new ObjectId(oid)))) {
				try (FileWriter fw = new FileWriter(optionalTemp)) {
					fw.write(d.toJson().toCharArray());
				}
				return optionalTemp;
			}

			console.printerrln("Could not find MongoDB document with object ID " + oid);
			return null;
		} catch (IOException ex) {
			console.printerrln("I/O exception while importing the MongoDB document with object ID " + oid);
			console.printerrln(ex);
			return null;
		}
	}

	@Override
	public void init(String vcsloc, IModelIndexer hawk) throws Exception {
		super.init(vcsloc, hawk);

		final URI mongoURI = new URI(vcsloc);
		String[] pathParts = mongoURI.getPath().split("/");
		if (pathParts.length != 3) {
			throw new IllegalArgumentException("URL should have /db/collection as its path");
		}
		this.mongoCollectionName = pathParts[2];
	}

	@Override
	public void run() throws Exception {
		super.run();

		this.mongoCollection = mongoDatabase.getCollection(mongoCollectionName);
		previousObjectIDs = new HashSet<>();
	}

	@Override
	public String getRepositoryPath(String rawPath) {
		// TODO Auto-generated method stub
		return rawPath;
	}

}
