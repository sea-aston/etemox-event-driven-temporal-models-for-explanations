/*******************************************************************************
 * Copyright (c) 2019 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.hawk.mqtt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.stream.Collectors;

import org.eclipse.hawk.core.IConsole;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.IVcsManager;
import org.eclipse.hawk.core.VcsChangeType;
import org.eclipse.hawk.core.VcsCommit;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.VcsRepositoryDelta;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Exposes an MQTT topic as a VCS to Hawk. The topic is mapped to a file with
 * the name of the topic. If the payloads for the topic are going to be JSON
 * documents, for instance, then the name of the topic should end in
 * {@code .json} so that the model parser components may recognise them.
 * 
 * <p>
 * Each new message is treated as a new revision of that file. The revision is
 * identified by the MQTT message ID. This connector does not do any parsing on
 * the actual message payload, so it should be reusable for any MQTT topic.
 * 
 * <p>
 * The URL should be in this form of (where "[]" indicates optional parts):
 * 
 * <code>tcp://host:port/mainTopic?extra=topic1,topic2...</code>
 *
 * TODO Free up old history in the MqttSubscriber - we may want to only keep a
 * window, or only keep messages until {@link #importFile(String, String, File)}
 * has been called on it.
 */
public class MqttConnector implements IVcsManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(MqttConnector.class);

	private static final String QUERY_EXTRA_PREFIX = "extra=";
	private static final String FIRST_REVISION = "0";

	// Controls how many syncs we can request from Hawk per second
	private static final int MAX_UPDATES_PER_SECOND = 5;
	private static final int MILLIS_UPDATE_INTERVAL = 1_000 / MAX_UPDATES_PER_SECOND;
	private long lastUpdateMillis;

	protected IConsole console;
	protected IModelIndexer indexer;

	private boolean isFrozen = false;
	private boolean isActive = false;

	private String username;
	private String password;
	private String brokerMqttURL;
	private String mqttHost;
	private int mqttPort;

	private List<String> topics = new ArrayList<>();
	private List<MqttSubscriber> subscribers = new ArrayList<>();

	public MqttConnector() {
		super();
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void init(String vcsloc, IModelIndexer hawk) throws Exception {
		this.indexer = hawk;
		this.brokerMqttURL = vcsloc;
		console = indexer.getConsole();

		final URI mqttURI = new URI(vcsloc);
		this.mqttHost = mqttURI.getHost();
		this.mqttPort = mqttURI.getPort();
		String[] pathParts = mqttURI.getPath().split("/");
		if (pathParts.length < 2) {
			throw new IllegalArgumentException(
					"URL should at least have /topic/ topic to susbcribe as its path (Exampe: tcp://localhost:1883/initial_setting.json?extra=Q_table_collection.json)");
		}
		topics.add(pathParts[1]);

		String query = mqttURI.getQuery();
		if (query.startsWith(QUERY_EXTRA_PREFIX)) {
			String[] extraTopics = query.substring(QUERY_EXTRA_PREFIX.length()).split(",");
			topics.addAll(Arrays.asList(extraTopics));
		}
	}

	@Override
	public void run() throws Exception {
		for (String topic : topics) {
			MqttSubscriber subscriber = new MqttSubscriber(mqttHost, Integer.toString(mqttPort), topic,
					this::messageReceived);
			subscribers.add(subscriber);
			subscriber.run();
		}

		isActive = true;
	}

	@Override
	public void shutdown() {
		for (MqttSubscriber subscriber : subscribers) {
			try {
				subscriber.shutdown();
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		isActive = false;
	}

	@Override
	public boolean isAuthSupported() {
		return true;
	}

	@Override
	public boolean isPathLocationAccepted() {
		return false;
	}

	@Override
	public boolean isURLLocationAccepted() {
		return true;
	}

	@Override
	public boolean isFrozen() {
		return isFrozen;
	}

	@Override
	public void setFrozen(boolean frozen) {
		isFrozen = frozen;
	}

	@Override
	public String getLocation() {
		return brokerMqttURL;
	}

	@Override
	public String getHumanReadableName() {
		return "MQTT Connector";
	}

	@Override
	public String getCurrentRevision() throws Exception {
		return subscribers.stream().map(s -> s.getCurrentRevision()).collect(Collectors.maxBy(Long::compareTo))
				.map(l -> l.toString()).orElseGet(() -> FIRST_REVISION);
	}

	@Override
	public String getFirstRevision() throws Exception {
		return FIRST_REVISION;
	}

	@Override
	public Collection<VcsCommitItem> getDelta(String startRevision) throws Exception {
		VcsRepositoryDelta delta = getDelta(startRevision, getCurrentRevision());
		return delta.getCompactedCommitItems();
	}

	@Override
	public VcsRepositoryDelta getDelta(String startRevision, String endRevision) throws Exception {
		final List<VcsCommit> commits = new ArrayList<>();

		for (MqttSubscriber subscriber : subscribers) {
			System.out.println("Number of subscribers>> " + subscribers.size());
			final NavigableMap<Long, byte[]> jsonLog = subscriber.getLog();
			if (!jsonLog.isEmpty()) {
				final Long startID = startRevision == null ? jsonLog.firstKey() : Long.parseLong(startRevision);
				final Long endID = endRevision == null ? jsonLog.lastKey() : Long.parseLong(endRevision);
				for (long id : jsonLog.subMap(startID, true, endID, true).keySet()) {
					addCommit(subscriber, id, commits);
				}
			}
		}

		VcsRepositoryDelta delta = new VcsRepositoryDelta(commits);
		delta.setManager(this);
		return delta;
	}

	private void addCommit(MqttSubscriber subscriber, long id, List<VcsCommit> commits) {
		VcsCommit commit = new VcsCommit();
		commit.setRevision(id + "");
		commit.setAuthor("none recorded");
		commit.setJavaDate(new Date(id/1000));
		commit.setMessage("none recorded");

		VcsCommitItem item = new VcsCommitItem();
		item.setChangeType(VcsChangeType.UPDATED);
		item.setCommit(commit);
		item.setPath(getSubscriberFilePath(subscriber));
		commit.getItems().add(item);

		commits.add(commit);
	}

	@Override
	public File importFile(String revision, String path, File optionalTemp) {
		final long revisionID = Long.parseLong(revision);

		for (MqttSubscriber subscriber : subscribers) {
			if (!getSubscriberFilePath(subscriber).equals(path) && !subscriber.getTopic().equals(path)) {
				continue;
			}

			// Get the most recent message at the time of revision
			final NavigableMap<Long, byte[]> log = subscriber.getLog();
			final Entry<Long, byte[]> ePayload = log.floorEntry(revisionID);
			if (ePayload != null) {
				byte[] payload = ePayload.getValue();
				try (FileOutputStream fos = new FileOutputStream(optionalTemp)) {
					fos.write(payload);
					return optionalTemp;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

		return null;
	}

	private String getSubscriberFilePath(MqttSubscriber subscriber) {
		return "/" + subscriber.getTopic();
	}

	@Override
	public String getRepositoryPath(String rawPath) {
		return rawPath;
	}

	@Override
	public void setCredentials(String username, String password, ICredentialsStore credStore) {
		// TODO Auto-generated method stub

	}

	private void messageReceived() {
		final long millisSinceUpdate = System.currentTimeMillis() - lastUpdateMillis;
		try {
			if (millisSinceUpdate >= MILLIS_UPDATE_INTERVAL) {
				indexer.requestImmediateSync();
			} else {
				indexer.scheduleTask(() -> {
					indexer.requestImmediateSync();
					return null;
				}, MILLIS_UPDATE_INTERVAL - millisSinceUpdate);
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		} finally {
			lastUpdateMillis = System.currentTimeMillis();
		}
	}

	public String getDefaultLocation() {
		return "tcp://host:port/topic";
	}

}