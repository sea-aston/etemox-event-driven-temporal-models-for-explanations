/**
 *
 * $Id$
 */
package uk.ac.aston.stormlog.validation;

import org.eclipse.emf.common.util.EList;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.NFRSatisfaction;

/**
 * A sample validator interface for {@link uk.ac.aston.stormlog.RewardTableRow}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface RewardTableRowValidator {
	boolean validate();

	boolean validateSatisfactions(EList<NFRSatisfaction> value);
	boolean validateAction(Action value);
	boolean validateValue(EList<Double> value);
}
