/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.Measure#getName <em>Name</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Measure#getThresholds <em>Thresholds</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getMeasure()
 * @model
 * @generated
 */
public interface Measure extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getMeasure_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Measure#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Thresholds</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Threshold}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thresholds</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getMeasure_Thresholds()
	 * @model containment="true"
	 * @generated
	 */
	EList<Threshold> getThresholds();

} // Measure
