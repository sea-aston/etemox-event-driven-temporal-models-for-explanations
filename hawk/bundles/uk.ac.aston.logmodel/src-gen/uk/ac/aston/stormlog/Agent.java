/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.Agent#getName <em>Name</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Agent#getObservations <em>Observations</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Agent#getDecisions <em>Decisions</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getAgent()
 * @model
 * @generated
 */
public interface Agent extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getAgent_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Agent#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Observations</b></em>' reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Observation}.
	 * It is bidirectional and its opposite is '{@link uk.ac.aston.stormlog.Observation#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observations</em>' reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getAgent_Observations()
	 * @see uk.ac.aston.stormlog.Observation#getAgent
	 * @model opposite="agent"
	 * @generated
	 */
	EList<Observation> getObservations();

	/**
	 * Returns the value of the '<em><b>Decisions</b></em>' reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Decision}.
	 * It is bidirectional and its opposite is '{@link uk.ac.aston.stormlog.Decision#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decisions</em>' reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getAgent_Decisions()
	 * @see uk.ac.aston.stormlog.Decision#getAgent
	 * @model opposite="agent"
	 * @generated
	 */
	EList<Decision> getDecisions();

} // Agent
