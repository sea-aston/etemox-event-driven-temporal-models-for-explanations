/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see uk.ac.aston.stormlog.StormlogPackage
 * @generated
 */
public interface StormlogFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StormlogFactory eINSTANCE = uk.ac.aston.stormlog.impl.StormlogFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Log</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Log</em>'.
	 * @generated
	 */
	Log createLog();

	/**
	 * Returns a new object of class '<em>Decision</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Decision</em>'.
	 * @generated
	 */
	Decision createDecision();

	/**
	 * Returns a new object of class '<em>Observation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Observation</em>'.
	 * @generated
	 */
	Observation createObservation();

	/**
	 * Returns a new object of class '<em>Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent</em>'.
	 * @generated
	 */
	Agent createAgent();

	/**
	 * Returns a new object of class '<em>Measure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Measure</em>'.
	 * @generated
	 */
	Measure createMeasure();

	/**
	 * Returns a new object of class '<em>Threshold</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Threshold</em>'.
	 * @generated
	 */
	Threshold createThreshold();

	/**
	 * Returns a new object of class '<em>Action Belief</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Belief</em>'.
	 * @generated
	 */
	ActionBelief createActionBelief();

	/**
	 * Returns a new object of class '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action</em>'.
	 * @generated
	 */
	Action createAction();

	/**
	 * Returns a new object of class '<em>NFR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NFR</em>'.
	 * @generated
	 */
	NFR createNFR();

	/**
	 * Returns a new object of class '<em>Reward Table</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reward Table</em>'.
	 * @generated
	 */
	RewardTable createRewardTable();

	/**
	 * Returns a new object of class '<em>NFR Satisfaction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NFR Satisfaction</em>'.
	 * @generated
	 */
	NFRSatisfaction createNFRSatisfaction();

	/**
	 * Returns a new object of class '<em>Reward Table Row</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reward Table Row</em>'.
	 * @generated
	 */
	RewardTableRow createRewardTableRow();

	/**
	 * Returns a new object of class '<em>Reward Table Threshold</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reward Table Threshold</em>'.
	 * @generated
	 */
	RewardTableThreshold createRewardTableThreshold();

	/**
	 * Returns a new object of class '<em>Double Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double Measurement</em>'.
	 * @generated
	 */
	DoubleMeasurement createDoubleMeasurement();

	/**
	 * Returns a new object of class '<em>Integer Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Measurement</em>'.
	 * @generated
	 */
	IntegerMeasurement createIntegerMeasurement();

	/**
	 * Returns a new object of class '<em>String Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Measurement</em>'.
	 * @generated
	 */
	StringMeasurement createStringMeasurement();

	/**
	 * Returns a new object of class '<em>Range Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Measurement</em>'.
	 * @generated
	 */
	RangeMeasurement createRangeMeasurement();

	/**
	 * Returns a new object of class '<em>Double List Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double List Measurement</em>'.
	 * @generated
	 */
	DoubleListMeasurement createDoubleListMeasurement();

	/**
	 * Returns a new object of class '<em>Integer List Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer List Measurement</em>'.
	 * @generated
	 */
	IntegerListMeasurement createIntegerListMeasurement();

	/**
	 * Returns a new object of class '<em>Boolean Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Measurement</em>'.
	 * @generated
	 */
	BooleanMeasurement createBooleanMeasurement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StormlogPackage getStormlogPackage();

} //StormlogFactory
